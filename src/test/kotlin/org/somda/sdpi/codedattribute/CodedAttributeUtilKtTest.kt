package org.somda.sdpi.codedattribute

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.sdc.biceps.model.extension.ExtensionType
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.biceps.model.participant.MdsState
import kotlin.test.assertNotNull

internal class CodedAttributeUtilKtTest {
    private val mdsDescriptor = MdsDescriptor()
    private val mdsState = MdsState()

    @BeforeEach
    fun setup() {
        mdsDescriptor.apply {
            extension = ExtensionType()
            extension!!.any.clear()
        }
        mdsState.apply {
            extension = ExtensionType()
            extension!!.any.clear()
        }
    }

    @Test
    fun testAddCodedStringAttribute() {
        assertNull(mdsDescriptor.codedAttributes())
        assertNull(mdsState.codedAttributes())

        mdsDescriptor.addCodedStringAttribute(
            CodedAttributeFactory.createString(
                TEST_STRING_VALUE,
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsDescriptor.addCodedStringAttribute(
            CodedAttributeFactory.createString(
                TEST_STRING_VALUE,
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), true
        )

        val descrAttrs = mdsDescriptor.codedAttributes()
        assertNotNull(descrAttrs)

        assertTrue(descrAttrs.value.isMustUnderstand)
        assertEquals(2, descrAttrs.value.codedStringAttribute.size)

        mdsState.addCodedStringAttribute(
            CodedAttributeFactory.createString(
                TEST_STRING_VALUE,
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsState.addCodedStringAttribute(
            CodedAttributeFactory.createString(
                TEST_STRING_VALUE,
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), false
        )

        val stateAttrs = mdsState.codedAttributes()
        assertNotNull(stateAttrs)

        assertFalse(stateAttrs.value.isMustUnderstand)
        assertEquals(2, stateAttrs.value.codedStringAttribute.size)
    }

    @Test
    fun testAddCodedIntegerAttribute() {
        assertNull(mdsDescriptor.codedAttributes())
        assertNull(mdsState.codedAttributes())

        mdsDescriptor.addCodedIntegerAttribute(
            CodedAttributeFactory.createInteger(
                TEST_NUMERIC_VALUE.toBigInteger(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsDescriptor.addCodedIntegerAttribute(
            CodedAttributeFactory.createInteger(
                TEST_NUMERIC_VALUE.toBigInteger(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), true
        )

        val descrAttrs = mdsDescriptor.codedAttributes()
        assertNotNull(descrAttrs)

        assertTrue(descrAttrs.value.isMustUnderstand)
        assertEquals(2, descrAttrs.value.codedIntegerAttribute.size)

        mdsState.addCodedIntegerAttribute(
            CodedAttributeFactory.createInteger(
                TEST_NUMERIC_VALUE.toBigInteger(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsState.addCodedIntegerAttribute(
            CodedAttributeFactory.createInteger(
                TEST_NUMERIC_VALUE.toBigInteger(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), false
        )

        val stateAttrs = mdsState.codedAttributes()
        assertNotNull(stateAttrs)

        assertFalse(stateAttrs.value.isMustUnderstand)
        assertEquals(2, stateAttrs.value.codedIntegerAttribute.size)
    }

    @Test
    fun testAddCodedDecimalAttribute() {
        assertNull(mdsDescriptor.codedAttributes())
        assertNull(mdsState.codedAttributes())

        mdsDescriptor.addCodedDecimalAttribute(
            CodedAttributeFactory.createDecimal(
                TEST_NUMERIC_VALUE.toBigDecimal(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsDescriptor.addCodedDecimalAttribute(
            CodedAttributeFactory.createDecimal(
                TEST_NUMERIC_VALUE.toBigDecimal(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), true
        )

        val descrAttrs = mdsDescriptor.codedAttributes()
        assertNotNull(descrAttrs)

        assertTrue(descrAttrs.value.isMustUnderstand)
        assertEquals(2, descrAttrs.value.codedDecimalAttribute.size)

        mdsState.addCodedDecimalAttribute(
            CodedAttributeFactory.createDecimal(
                TEST_NUMERIC_VALUE.toBigDecimal(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsState.addCodedDecimalAttribute(
            CodedAttributeFactory.createDecimal(
                TEST_NUMERIC_VALUE.toBigDecimal(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), false
        )

        val stateAttrs = mdsState.codedAttributes()
        assertNotNull(stateAttrs)

        assertFalse(stateAttrs.value.isMustUnderstand)
        assertEquals(2, stateAttrs.value.codedDecimalAttribute.size)
    }

    @Test
    fun testMixed() {
        assertNull(mdsDescriptor.codedAttributes())

        mdsDescriptor.addCodedDecimalAttribute(
            CodedAttributeFactory.createDecimal(
                TEST_NUMERIC_VALUE.toBigDecimal(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        mdsDescriptor.addCodedIntegerAttribute(
            CodedAttributeFactory.createInteger(
                TEST_NUMERIC_VALUE.toBigInteger(),
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            ), true
        )

        mdsDescriptor.addCodedStringAttribute(
            CodedAttributeFactory.createString(
                TEST_STRING_VALUE,
                TEST_CODE,
                TEST_CODING_SYSTEM,
                TEST_CODING_SYSTEM_VERSION,
                TEST_SYMBOLIC_CODE_NAME
            )
        )

        val descrAttrs = mdsDescriptor.codedAttributes()
        assertNotNull(descrAttrs)

        assertTrue(descrAttrs.value.isMustUnderstand)
        assertEquals(1, descrAttrs.value.codedStringAttribute.size)
        assertEquals(1, descrAttrs.value.codedIntegerAttribute.size)
        assertEquals(1, descrAttrs.value.codedDecimalAttribute.size)
    }

    companion object {
        const val TEST_STRING_VALUE = "test-value"
        const val TEST_NUMERIC_VALUE = 100
        const val TEST_CODE = "test-code"
        const val TEST_CODING_SYSTEM = "test-coding-system"
        const val TEST_CODING_SYSTEM_VERSION = "test-coding-system-version"
        const val TEST_SYMBOLIC_CODE_NAME = "test-symbolic-code-name"
    }
}