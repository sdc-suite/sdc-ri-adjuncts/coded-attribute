package org.somda.sdpi.discovery.managedmode

import com.google.common.eventbus.Subscribe
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.assistedinject.FactoryModuleBuilder
import com.google.inject.util.Modules
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.protosdc.sdc.dpws.DpwsConfig
import org.somda.protosdc.sdc.dpws.dagger.DaggerDpwsComponent
import org.somda.protosdc.sdc.dpws.soap.client.HttpClientConfig
import org.somda.protosdc.sdc.dpws.soap.discovery.proxy.DiscoveryProxy
import org.somda.protosdc.sdc.dpws.soap.discovery.proxy.DiscoveryProxyConfig
import org.somda.protosdc.sdc.dpws.soap.server.HttpServerConfig
import org.somda.sdc.common.guice.DefaultCommonConfigModule
import org.somda.sdc.common.guice.DefaultCommonModule
import org.somda.sdc.dpws.CommunicationLog
import org.somda.sdc.dpws.CommunicationLogImpl
import org.somda.sdc.dpws.DpwsConstants
import org.somda.sdc.dpws.DpwsFramework
import org.somda.sdc.dpws.client.Client
import org.somda.sdc.dpws.client.DiscoveredDevice
import org.somda.sdc.dpws.client.DiscoveryFilter
import org.somda.sdc.dpws.client.DiscoveryObserver
import org.somda.sdc.dpws.client.event.DeviceEnteredMessage
import org.somda.sdc.dpws.client.event.DeviceLeftMessage
import org.somda.sdc.dpws.device.Device
import org.somda.sdc.dpws.device.DeviceSettings
import org.somda.sdc.dpws.device.factory.DeviceFactory
import org.somda.sdc.dpws.factory.CommunicationLogFactory
import org.somda.sdc.dpws.guice.DefaultDpwsConfigModule
import org.somda.sdc.dpws.guice.DefaultDpwsModule
import org.somda.sdc.dpws.soap.SoapUtil
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsaddressing.model.EndpointReferenceType
import org.somda.sdpi.discovery.managedmode.factory.ManagedModeFactory
import org.somda.sdpi.discovery.managedmode.guice.ManagedModeModule
import java.net.InetAddress
import java.net.NetworkInterface
import java.time.Duration
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

class ManagedModeTest {
    lateinit var discoProxy: DiscoveryProxyService
    lateinit var discoveryProxyUrl: String

    lateinit var clientInjector: Injector
    lateinit var targetServiceInjector: Injector

    lateinit var targetServiceFramework: DpwsFramework
    lateinit var clientFramework: DpwsFramework

    @BeforeEach
    fun beforeEach() {
        discoProxy = createDiscoveryProxyService().apply { start() }
        discoveryProxyUrl = discoProxy.url().toString()
        clientInjector = IntegrationTestUtil.createInjector()
        targetServiceInjector = IntegrationTestUtil.createInjector()

        targetServiceFramework = targetServiceInjector.getInstance(DpwsFramework::class.java).apply {
            setNetworkInterface(networkInterface)
            startAsync().awaitRunning()
        }
        clientFramework = clientInjector.getInstance(DpwsFramework::class.java).apply {
            setNetworkInterface(networkInterface)
            startAsync().awaitRunning()
        }
    }

    @AfterEach
    fun afterEach() {
        targetServiceFramework.stopAsync().awaitTerminated()
        clientFramework.stopAsync().awaitTerminated()
        discoProxy.stop()
    }

    @Test
    fun `probe and resolve`() {
        val device = createDevice(targetServiceInjector)
        val discoveryMetadata = DiscoveryMetadata(
            scopes = listOf("scope"),
            types = listOf(DpwsConstants.DEVICE_TYPE),
            xAddrs = listOf("xAddr")
        )
        val managedModeTargetService =
            targetServiceInjector.getInstance(ManagedModeFactory::class.java).createTargetService(
                discoveryProxyUrl,
                device.eprAddress,
                discoveryMetadata
            )

        device.startAsync().awaitRunning()
        managedModeTargetService.startAsync().awaitRunning()

        val client = clientInjector.getInstance(Client::class.java)
        client.startAsync().awaitRunning()

        val managedModeClient =
            clientInjector.getInstance(ManagedModeFactory::class.java).createClient(discoveryProxyUrl)
        managedModeClient.startAsync().awaitRunning()

        val resolved = managedModeClient.resolve(device.eprAddress)
            .get(IntegrationTestUtil.MAX_WAIT_TIME.seconds, TimeUnit.SECONDS)

        requireNotNull(resolved)
        assertEquals(device.eprAddress, resolved.eprAddress)
        assertEquals(discoveryMetadata.scopes!!.joinToString(), resolved.scopes.joinToString())
        assertEquals(discoveryMetadata.types!!.joinToString(), resolved.types.joinToString())
        assertEquals(discoveryMetadata.xAddrs!!.joinToString(), resolved.xAddrs.joinToString())

        val probed = managedModeClient.probe(DiscoveryFilter(listOf(), discoveryMetadata.scopes))
            .get(IntegrationTestUtil.MAX_WAIT_TIME.seconds, TimeUnit.SECONDS)

        assertEquals(1, probed.size)
        val probeResult = probed.first()

        assertEquals(device.eprAddress, probeResult.eprAddress)
        assertEquals(discoveryMetadata.scopes!!.joinToString(), probeResult.scopes.joinToString())
        assertEquals(discoveryMetadata.types!!.joinToString(), probeResult.types.joinToString())
        assertEquals(discoveryMetadata.xAddrs!!.joinToString(), probeResult.xAddrs.joinToString())

        managedModeTargetService.stopAsync().awaitTerminated()
        managedModeClient.stopAsync().awaitTerminated()
        device.stopAsync().awaitTerminated()
        client.stopAsync().awaitTerminated()
    }

    @Test
    fun `hello and bye`() {
        val device = createDevice(targetServiceInjector)
        val discoveryMetadata = DiscoveryMetadata(
            scopes = listOf("scope"),
            types = listOf(DpwsConstants.DEVICE_TYPE),
            xAddrs = listOf("xAddr")
        )
        val managedModeTargetService =
            targetServiceInjector.getInstance(ManagedModeFactory::class.java).createTargetService(
                discoveryProxyUrl,
                device.eprAddress,
                discoveryMetadata
            )

        device.startAsync().awaitRunning()

        val client = clientInjector.getInstance(Client::class.java)
        client.startAsync().awaitRunning()

        val managedModeClient =
            clientInjector.getInstance(ManagedModeFactory::class.java).createClient(discoveryProxyUrl)
        managedModeClient.startAsync().awaitRunning()

        val discoverySpy = DiscoveryObserverSpy()

        managedModeClient.registerDiscoveryObserver(discoverySpy)

        managedModeTargetService.startAsync().awaitRunning()

        val discoveredDevice = discoverySpy.waitForEnteredDevice()

        assertEquals(device.eprAddress, discoveredDevice.eprAddress)
        assertEquals(discoveryMetadata.scopes!!.joinToString(), discoveredDevice.scopes.joinToString())
        assertEquals(discoveryMetadata.types!!.joinToString(), discoveredDevice.types.joinToString())
        assertEquals(discoveryMetadata.xAddrs!!.joinToString(), discoveredDevice.xAddrs.joinToString())

        managedModeTargetService.stopAsync().awaitTerminated()

        val leftDevice = discoverySpy.waitForLeftDevice()

        assertEquals(leftDevice, discoveredDevice.eprAddress)

        managedModeClient.stopAsync().awaitTerminated()
        device.stopAsync().awaitTerminated()
        client.stopAsync().awaitTerminated()
    }

    private fun createDevice(injector: Injector): Device {
        val eprAddress = injector.getInstance(SoapUtil::class.java).createUriFromUuid(UUID.randomUUID())
        val wsaUtil = injector.getInstance(WsAddressingUtil::class.java)
        val epr = wsaUtil.createEprWithAddress(eprAddress)

        val deviceSettings = object : DeviceSettings {
            override fun getEndpointReference(): EndpointReferenceType {
                return epr
            }

            override fun getNetworkInterface(): NetworkInterface {
                try {
                    return NetworkInterface.getByInetAddress(InetAddress.getByName("127.0.0.1"))
                } catch (e: Exception) {
                    throw RuntimeException(e)
                }
            }
        }

        return injector.getInstance(DeviceFactory::class.java).createDevice(deviceSettings)
    }

    companion object {
        val networkInterface = NetworkInterface.getByInetAddress(InetAddress.getByName("127.0.0.1"))
    }
}

private fun createDiscoveryProxyService(): DiscoveryProxyService = runBlocking {
    val component = DaggerDpwsComponent.builder().bind(
        DpwsConfig(
            httpClient = HttpClientConfig(
                disableHttps = true
            ),
            httpServer = HttpServerConfig(
                enableHttp = true,
                disableHttps = true
            )
        )

    ).build()

    val discoProxyComponent = component.discoveryProxyComponentBuilder().bind(
        DiscoveryProxyConfig()
    ).build()
    val discoProxy = discoProxyComponent.discoveryProxy()
    DiscoveryProxyService(discoProxy)
}

class DiscoveryProxyService(private val discoProxy: DiscoveryProxy) {
    fun start() = runBlocking { discoProxy.start() }
    fun stop() = runBlocking { discoProxy.stop() }
    fun url() = runBlocking { discoProxy.httpConnectorUrl()!! }
}

object IntegrationTestUtil {
    fun createInjector(): Injector = Guice.createInjector(
        Modules.override(
            DefaultCommonConfigModule(),
            DefaultDpwsModule(),
            DefaultCommonModule(),
            DefaultDpwsConfigModule(),
            ManagedModeModule()
        ).with(
            object : AbstractModule() {
                override fun configure() {
                    install(
                        FactoryModuleBuilder()
                            .implement(CommunicationLog::class.java, CommunicationLogImpl::class.java)
                            .build(CommunicationLogFactory::class.java)
                    )
                }
            }
        )
    )

    private val LOG: Logger = LogManager.getLogger(
        IntegrationTestUtil::class.java
    )
    var MAX_WAIT_TIME: Duration = Duration.ofSeconds(10)

    fun preferIpV4Usage() {
        System.setProperty("java.net.preferIPv4Stack", "true")
    }
}

class DiscoveryObserverSpy : DiscoveryObserver {
    val helloFuture = CompletableFuture<DiscoveredDevice>()
    val byeFuture = CompletableFuture<String>()

    fun waitForEnteredDevice(): DiscoveredDevice {
        return helloFuture.get(IntegrationTestUtil.MAX_WAIT_TIME.seconds, TimeUnit.SECONDS)
    }

    fun waitForLeftDevice(): String {
        return byeFuture.get(IntegrationTestUtil.MAX_WAIT_TIME.seconds, TimeUnit.SECONDS)
    }

    @Subscribe
    fun onHello(deviceEnteredMessage: DeviceEnteredMessage) {
        helloFuture.complete(deviceEnteredMessage.payload)
    }

    @Subscribe
    fun onBye(deviceLeftMessage: DeviceLeftMessage) {
        byeFuture.complete(deviceLeftMessage.payload)
    }
}