package org.somda.sdpi.discovery.managedmode

import com.google.common.util.concurrent.Service
import javax.xml.namespace.QName

/**
 * Discovery metadata as specified by WS-Discovery.
 */
data class DiscoveryMetadata(
    val types: List<QName>? = null,
    val scopes: List<String>? = null,
    val xAddrs: List<String>? = null
)

/**
 * WS-Discovery managed mode target service access.
 *
 * Sends a hello message to the discovery proxy when started. Sends a bye message to the discovery proxy when stopped.
 */
interface ManagedModeTargetService : Service {
    /**
     * Sends a hello message to the discovery proxy that contains the given discovery metadata.
     *
     * @param updatedMetadata the metadata to update.
     */
    fun update(updatedMetadata: DiscoveryMetadata)
}