package org.somda.sdpi.discovery.managedmode.guice

import com.google.inject.AbstractModule
import com.google.inject.assistedinject.FactoryModuleBuilder
import org.somda.sdpi.discovery.managedmode.ManagedModeClient
import org.somda.sdpi.discovery.managedmode.ManagedModeClientImpl
import org.somda.sdpi.discovery.managedmode.ManagedModeTargetService
import org.somda.sdpi.discovery.managedmode.ManagedModeTargetServiceImpl
import org.somda.sdpi.discovery.managedmode.factory.ManagedModeFactory

/**
 * Guice model to be used to enable discovery proxy instantiation.
 *
 * Note that the managed mode module does only run when DPWS bindings are loaded as well:
 *
 * - [org.somda.sdc.common.guice.DefaultCommonConfigModule]
 * - [org.somda.sdc.dpws.guice.DefaultDpwsModule]
 * - [org.somda.sdc.common.guice.DefaultCommonModule]
 * - [org.somda.sdc.dpws.guice.DefaultDpwsConfigModule]
 */
class ManagedModeModule : AbstractModule() {
    override fun configure() {
        install(
            FactoryModuleBuilder()
                .implement(ManagedModeClient::class.java, ManagedModeClientImpl::class.java)
                .implement(ManagedModeTargetService::class.java, ManagedModeTargetServiceImpl::class.java)
                .build(ManagedModeFactory::class.java)
        )
    }
}