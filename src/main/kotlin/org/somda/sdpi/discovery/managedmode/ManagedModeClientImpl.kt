package org.somda.sdpi.discovery.managedmode

import com.google.common.eventbus.EventBus
import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.ListeningExecutorService
import com.google.inject.assistedinject.Assisted
import com.google.inject.assistedinject.AssistedInject
import org.somda.sdc.common.util.ExecutorWrapperService
import org.somda.sdc.dpws.client.DiscoveredDevice
import org.somda.sdc.dpws.client.DiscoveryFilter
import org.somda.sdc.dpws.client.DiscoveryObserver
import org.somda.sdc.dpws.client.event.DeviceEnteredMessage
import org.somda.sdc.dpws.client.event.DeviceLeftMessage
import org.somda.sdc.dpws.factory.TransportBindingFactory
import org.somda.sdc.dpws.guice.AppDelayExecutor
import org.somda.sdc.dpws.guice.ClientSpecific
import org.somda.sdc.dpws.guice.WsDiscovery
import org.somda.sdc.dpws.http.HttpUriBuilder
import org.somda.sdc.dpws.network.LocalAddressResolver
import org.somda.sdc.dpws.soap.SoapMessage
import org.somda.sdc.dpws.soap.SoapUtil
import org.somda.sdc.dpws.soap.factory.NotificationSinkFactory
import org.somda.sdc.dpws.soap.factory.RequestResponseClientFactory
import org.somda.sdc.dpws.soap.interception.Interceptor
import org.somda.sdc.dpws.soap.interception.MessageInterceptor
import org.somda.sdc.dpws.soap.interception.NotificationObject
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingServerInterceptor
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsdiscovery.MatchBy
import org.somda.sdc.dpws.soap.wsdiscovery.WsDiscoveryConstants
import org.somda.sdc.dpws.soap.wsdiscovery.model.ByeType
import org.somda.sdc.dpws.soap.wsdiscovery.model.HelloType
import org.somda.sdc.dpws.soap.wsdiscovery.model.ObjectFactory
import org.somda.sdc.dpws.soap.wsdiscovery.model.ProbeMatchesType
import org.somda.sdc.dpws.soap.wsdiscovery.model.ResolveMatchesType
import org.somda.sdc.dpws.soap.wseventing.EventSink
import org.somda.sdc.dpws.soap.wseventing.SubscribeResult
import org.somda.sdc.dpws.soap.wseventing.factory.WsEventingEventSinkFactory
import java.net.URI
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import javax.xml.namespace.QName

internal class ManagedModeClientImpl @AssistedInject constructor(
    @Assisted val discoveryProxyUrl: String,
    private val uriBuilder: HttpUriBuilder,
    private val wsdFactory: ObjectFactory,
    private val soapUtil: SoapUtil,
    private val wsAddressingUtil: WsAddressingUtil,
    private val wsEventingEventSinkFactory: WsEventingEventSinkFactory,
    private val localAddressResolver: LocalAddressResolver,
    private val notificationSinkFactory: NotificationSinkFactory,
    private val discoveryBus: EventBus,
    @ClientSpecific val wsAddressingServerInterceptor: WsAddressingServerInterceptor,
    @WsDiscovery val executorService: ExecutorWrapperService<ListeningExecutorService>,
    @AppDelayExecutor private val watchdogExecutor: ExecutorWrapperService<ScheduledExecutorService>,
    requestResponseClientFactory: RequestResponseClientFactory,
    transportBindingFactory: TransportBindingFactory
) : ManagedModeClient, AbstractIdleService() {
    private val transportBinding = transportBindingFactory.createTransportBinding(discoveryProxyUrl, null)
    private val requestResponseClient = requestResponseClientFactory.createRequestResponseClient(transportBinding)
    private lateinit var wsEventingEventSink: EventSink
    private lateinit var subscription: SubscribeResult

    override fun startUp() {
        val httpBinding = uriBuilder.buildUri(
            URI.create(discoveryProxyUrl).scheme,
            localAddressResolver.getLocalAddress(discoveryProxyUrl).orElseThrow(),
            0
        )
        wsEventingEventSink = wsEventingEventSinkFactory.createWsEventingEventSink(
            requestResponseClient,
            httpBinding,
            null
        )

        val notificationSink = notificationSinkFactory.createNotificationSink(wsAddressingServerInterceptor)
        notificationSink.register(
            object : Interceptor {
                @MessageInterceptor(WsDiscoveryConstants.WSA_ACTION_HELLO)
                fun processResolveMatches(notificationObject: NotificationObject) {
                    soapUtil.getBody(notificationObject.notification, HelloType::class.java).orElseThrow().also {
                        discoveryBus.post(
                            DeviceEnteredMessage(
                                DiscoveredDevice(
                                    it.endpointReference!!.address!!.value,
                                    it.types ?: listOf(),
                                    it.scopes?.value ?: listOf(),
                                    it.xAddrs,
                                    it.metadataVersion
                                )
                            )
                        )
                    }
                }

                @MessageInterceptor(WsDiscoveryConstants.WSA_ACTION_BYE)
                fun processProbeMatches(notificationObject: NotificationObject) {
                    soapUtil.getBody(notificationObject.notification, ByeType::class.java).orElseThrow().also {
                        discoveryBus.post(
                            DeviceLeftMessage(it.endpointReference!!.address!!.value)
                        )
                    }
                }
            }
        )

        subscription = wsEventingEventSink.subscribe(
            FILTER_DIALECT,
            listOf(),
            null,
            notificationSink
        ).get()

        watchdogExecutor.get().schedule(
            AutoRenew(watchdogExecutor, wsEventingEventSink, subscription.subscriptionId),
            subscription.grantedExpires.toMillis() - ONE_MINUTE_IN_MILLIS,
            TimeUnit.MILLISECONDS
        )
    }

    override fun shutDown() {
        wsEventingEventSink.unsubscribe(subscription.subscriptionId)
    }

    override fun registerDiscoveryObserver(observer: DiscoveryObserver) {
        discoveryBus.register(observer)
    }

    override fun unregisterDiscoveryObserver(observer: DiscoveryObserver) {
        discoveryBus.unregister(observer)
    }

    override fun probe(discoveryFilter: DiscoveryFilter): ListenableFuture<List<DiscoveredDevice>> {
        return executorService.get().submit<List<DiscoveredDevice>> {
            val response = requestResponseClient.sendRequestResponse(
                createProbeMessage(
                    discoveryFilter.types,
                    discoveryFilter.scopes,
                    discoveryFilter.matchBy
                )
            )

            soapUtil.getBody(response, ProbeMatchesType::class.java)
                .orElseThrow().let { probeMatchesType ->
                    probeMatchesType.probeMatch?.map {
                        DiscoveredDevice(
                            it.endpointReference.address.value,
                            it.types ?: listOf(),
                            it.scopes?.value ?: listOf(),
                            it.xAddrs ?: listOf(),
                            it.metadataVersion
                        )
                    } ?: listOf()
                }
        }
    }

    override fun resolve(eprAddress: String): ListenableFuture<DiscoveredDevice?> {
        return executorService.get().submit<DiscoveredDevice?> {
            val response = requestResponseClient.sendRequestResponse(
                createResolveMessage(eprAddress)
            )

            soapUtil.getBody(response, ResolveMatchesType::class.java)
                .orElseThrow().let { resolveMatchesType ->
                    resolveMatchesType.resolveMatch?.let {
                        DiscoveredDevice(
                            it.endpointReference.address.value,
                            it.types ?: listOf(),
                            it.scopes?.value ?: listOf(),
                            it.xAddrs ?: listOf(),
                            it.metadataVersion
                        )
                    }
                }
        }
    }

    private fun createProbeMessage(
        types: Collection<QName>,
        scopes: Collection<String>,
        matchBy: MatchBy?
    ): SoapMessage {
        val scopesType = wsdFactory.createScopesType().apply {
            if (matchBy != null) {
                this.matchBy = matchBy.uri
            }
            this.value = ArrayList(scopes)
        }

        val probeType = wsdFactory.createProbeType().apply {
            this.types = ArrayList(types)
            this.scopes = scopesType
        }

        return soapUtil.createMessage(
            WsDiscoveryConstants.WSA_ACTION_PROBE,
            WsDiscoveryConstants.WSA_UDP_TO,
            wsdFactory.createProbe(probeType)
        )
    }

    private fun createResolveMessage(
        eprAddress: String
    ): SoapMessage {
        val resolveType = wsdFactory.createResolveType().apply {
            this.endpointReference = wsAddressingUtil.createEprWithAddress(eprAddress)
        }

        return soapUtil.createMessage(
            WsDiscoveryConstants.WSA_ACTION_RESOLVE,
            WsDiscoveryConstants.WSA_UDP_TO,
            wsdFactory.createResolve(resolveType)
        )
    }

    private companion object {
        const val FILTER_DIALECT = "http://discoproxy"
        const val ONE_MINUTE_IN_MILLIS = 60 * 1000
    }

    private class AutoRenew(
        private val executorService: ExecutorWrapperService<ScheduledExecutorService>,
        private val eventSink: EventSink,
        private val subscriptionId: String
    ) : Runnable {
        override fun run() {
            val duration = eventSink.renew(subscriptionId, null).get()
            if (executorService.isRunning) {
                executorService.get().schedule(
                    AutoRenew(executorService, eventSink, subscriptionId),
                    duration.toMillis() - ONE_MINUTE_IN_MILLIS,
                    TimeUnit.MILLISECONDS
                )
            }
        }
    }
}