package org.somda.sdpi.discovery.managedmode

import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.Service
import org.somda.sdc.dpws.client.DiscoveredDevice
import org.somda.sdc.dpws.client.DiscoveryFilter
import org.somda.sdc.dpws.client.DiscoveryObserver
import org.somda.sdc.dpws.soap.exception.TransportException
import org.somda.sdc.dpws.soap.interception.InterceptorException

/**
 * WS-Discovery managed mode client access.
 */
interface ManagedModeClient : Service, DiscoveryObserver {
    /**
     * Subscribes to discovery events.
     *
     * @param observer the observer that is supposed to receive events.
     */
    fun registerDiscoveryObserver(observer: DiscoveryObserver)

    /**
     * Unsubscribes from discovery events.
     *
     * @param observer the observer that shall not receive events anymore.
     */
    fun unregisterDiscoveryObserver(observer: DiscoveryObserver)

    /**
     * Probes for devices by using the configured discovery proxy.
     *
     * This method asynchronously sends a WS-Discovery Probe to the discovery proxy.
     *
     * @param discoveryFilter types and scopes the discovery process shall filter against.
     * @throws TransportException   if probe cannot be sent.
     * @throws InterceptorException if one of the interceptors causes a failure.
     */
    @Throws(TransportException::class, InterceptorException::class)
    fun probe(discoveryFilter: DiscoveryFilter): ListenableFuture<List<DiscoveredDevice>>

    /**
     * Resolves physical addresses (XAddrs) of a device by using the configured discovery proxy.
     *
     * This method is an asynchronous unidirectional call.
     *
     * @param eprAddress the endpoint reference address of the device to resolve.
     * @return a future that holds the result of the resolve (or null, if no EPR match was found).
     * @throws TransportException   if resolve cannot be sent.
     * @throws InterceptorException if one of the interceptors causes a failure.
     */
    @Throws(TransportException::class, InterceptorException::class)
    fun resolve(eprAddress: String): ListenableFuture<DiscoveredDevice?>
}