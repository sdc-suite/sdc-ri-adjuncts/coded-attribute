package org.somda.sdpi.discovery.managedmode.factory

import com.google.inject.assistedinject.Assisted
import org.somda.sdpi.discovery.managedmode.DiscoveryMetadata
import org.somda.sdpi.discovery.managedmode.ManagedModeClient
import org.somda.sdpi.discovery.managedmode.ManagedModeTargetService

/**
 * Factory interface to create managed mode target services and clients.
 */
interface ManagedModeFactory {
    /**
     * Creates a managed mode client.
     *
     * @param discoveryProxyUrl the URL of the discovery proxy to use.
     */
    fun createClient(
        @Assisted discoveryProxyUrl: String
    ): ManagedModeClient

    /**
     * Creates a managed mode target service.
     *
     * @param discoveryProxyUrl the URL of the discovery proxy to use.
     * @param eprAddress the EPR address property of the target service.
     * @param initialDiscoveryMetadata initial metadata to be used for the hello on service startup.
     */
    fun createTargetService(
        @Assisted("discoveryProxyUrl") discoveryProxyUrl: String,
        @Assisted("eprAddress") eprAddress: String,
        @Assisted initialDiscoveryMetadata: DiscoveryMetadata
    ): ManagedModeTargetService

}