package org.somda.sdpi.discovery.managedmode

import com.google.common.primitives.UnsignedInteger
import com.google.common.util.concurrent.AbstractIdleService
import com.google.inject.assistedinject.Assisted
import com.google.inject.assistedinject.AssistedInject
import org.somda.sdc.dpws.factory.TransportBindingFactory
import org.somda.sdc.dpws.soap.SoapMessage
import org.somda.sdc.dpws.soap.SoapUtil
import org.somda.sdc.dpws.soap.factory.RequestResponseClientFactory
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsdiscovery.WsDiscoveryConstants
import org.somda.sdc.dpws.soap.wsdiscovery.model.ObjectFactory
import java.time.Instant
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

internal class ManagedModeTargetServiceImpl @AssistedInject constructor(
    @Assisted("discoveryProxyUrl") val discoveryProxyUrl: String,
    @Assisted("eprAddress") val eprAddress: String,
    @Assisted val initialDiscoveryMetadata: DiscoveryMetadata,
    private val wsdFactory: ObjectFactory,
    private val soapUtil: SoapUtil,
    wsaUtil: WsAddressingUtil,
    requestResponseClientFactory: RequestResponseClientFactory,
    transportBindingFactory: TransportBindingFactory
) : ManagedModeTargetService, AbstractIdleService() {
    private val lock = ReentrantLock()
    private var metadataVersion = nextMetadataVersion()
    private val epr = wsaUtil.createEprWithAddress(eprAddress)
    private val transportBinding = transportBindingFactory.createTransportBinding(discoveryProxyUrl, null)
    private val requestResponseClient = requestResponseClientFactory.createRequestResponseClient(transportBinding)

    override fun startUp() {
        try {
            requestResponseClient.sendRequestResponse(
                createHelloMessage(
                    initialDiscoveryMetadata,
                    incMetadataVersionAndGet()
                )
            )
        } catch (e: Exception) {
            println(e.message)
        }
    }

    override fun shutDown() {
        requestResponseClient.sendRequestResponse(createByeMessage())
    }

    override fun update(updatedMetadata: DiscoveryMetadata) {
        requestResponseClient.sendRequestResponse(
            createHelloMessage(
                updatedMetadata,
                incMetadataVersionAndGet()
            )
        )
    }

    private fun createHelloMessage(
        discoveryMetadata: DiscoveryMetadata,
        metadataVersion: UnsignedInteger
    ): SoapMessage {
        val helloType = wsdFactory.createHelloType().apply {
            endpointReference = epr
            xAddrs = discoveryMetadata.xAddrs
            scopes = wsdFactory.createScopesType().apply {
                value = discoveryMetadata.scopes
            }
            types = discoveryMetadata.types
            this.metadataVersion = metadataVersion.toLong()
        }

        return soapUtil.createMessage(WsDiscoveryConstants.WSA_ACTION_HELLO, wsdFactory.createHello(helloType))
    }

    private fun createByeMessage(): SoapMessage {
        val byeType = wsdFactory.createByeType().apply {
            endpointReference = epr
        }

        return soapUtil.createMessage(WsDiscoveryConstants.WSA_ACTION_BYE, wsdFactory.createBye(byeType))
    }

    private fun incMetadataVersionAndGet(): UnsignedInteger = lock.withLock {
        metadataVersion = nextMetadataVersion(metadataVersion)
        return UnsignedInteger.valueOf(metadataVersion.toLong())
    }

    private fun nextMetadataVersion(currentVersion: UnsignedInteger? = null): UnsignedInteger {
        // Metadata version is calculated from timestamp in seconds
        val newVersion = UnsignedInteger.valueOf(Instant.now().toEpochMilli() / 1000L)
        if (currentVersion == null) {
            return newVersion
        }

        // If there is more than one metadata version increment within one second just increment the current version.
        // This approach does not scale if there are tons of changes to the metadata version in a short time.
        // For this unlikely scenario an implementation is required that can persist metadata versions
        if (newVersion <= currentVersion) {
            return currentVersion.plus(UnsignedInteger.ONE)
        }

        return newVersion
    }
}