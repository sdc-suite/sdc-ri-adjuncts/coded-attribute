package org.somda.sdpi.codedattribute

import org.somda.sdc.biceps.model.extension.ExtensionType
import org.somda.sdc.biceps.model.participant.AbstractDescriptor
import org.somda.sdc.biceps.model.participant.AbstractState
import org.somda.sdpi.model.*
import java.math.BigDecimal
import java.math.BigInteger
import jakarta.xml.bind.JAXBElement

object CodedAttributeFactory {
    fun createString(
        value: String,
        code: String,
        codingSystem: String? = null,
        codingSystemVersion: String? = null,
        symbolicCodeName: String? = null
    ): CodedStringAttributeType = CodedStringAttributeType().apply {
        mdcAttribute = MdcAttributeType().apply {
            this.code = code
            this.codingSystem = codingSystem
            this.codingSystemVersion = codingSystemVersion
            this.symbolicCodeName = symbolicCodeName
        }
        this.value = value
    }

    fun createInteger(
        value: BigInteger,
        code: String,
        codingSystem: String? = null,
        codingSystemVersion: String? = null,
        symbolicCodeName: String? = null
    ): CodedIntegerAttributeType = CodedIntegerAttributeType().apply {
        mdcAttribute = MdcAttributeType().apply {
            this.code = code
            this.codingSystem = codingSystem
            this.codingSystemVersion = codingSystemVersion
            this.symbolicCodeName = symbolicCodeName
        }
        this.value = value
    }

    fun createDecimal(
        value: BigDecimal,
        code: String,
        codingSystem: String? = null,
        codingSystemVersion: String? = null,
        symbolicCodeName: String? = null
    ): CodedDecimalAttributeType = CodedDecimalAttributeType().apply {
        mdcAttribute = MdcAttributeType().apply {
            this.code = code
            this.codingSystem = codingSystem
            this.codingSystemVersion = codingSystemVersion
            this.symbolicCodeName = symbolicCodeName
        }
        this.value = value
    }
}

fun AbstractDescriptor.addCodedStringAttribute(
    codedStringAttribute: CodedStringAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedStringAttribute(checkNotNull(this.extension), codedStringAttribute, mustUnderstand)
}

fun AbstractDescriptor.addCodedIntegerAttribute(
    codedIntegerAttribute: CodedIntegerAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedIntegerAttribute(checkNotNull(this.extension), codedIntegerAttribute, mustUnderstand)
}

fun AbstractDescriptor.addCodedDecimalAttribute(
    codedDecimalAttribute: CodedDecimalAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedDecimalAttribute(checkNotNull(this.extension), codedDecimalAttribute, mustUnderstand)
}

fun AbstractState.addCodedStringAttribute(
    codedStringAttribute: CodedStringAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedStringAttribute(checkNotNull(this.extension), codedStringAttribute, mustUnderstand)
}

fun AbstractState.addCodedIntegerAttribute(
    codedIntegerAttribute: CodedIntegerAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedIntegerAttribute(checkNotNull(this.extension), codedIntegerAttribute, mustUnderstand)
}

fun AbstractState.addCodedDecimalAttribute(
    codedDecimalAttribute: CodedDecimalAttributeType,
    mustUnderstand: Boolean = false
) {
    if (this.extension == null) {
        this.extension = org.somda.sdc.biceps.model.extension.ObjectFactory().createExtensionType()
    }
    addCodedDecimalAttribute(checkNotNull(this.extension), codedDecimalAttribute, mustUnderstand)
}

fun AbstractDescriptor.codedAttributes(): JAXBElement<CodedAttributesType>? =
    findCodedAttributes(this.extension?.any ?: listOf())

fun AbstractState.codedAttributes(): JAXBElement<CodedAttributesType>? =
    findCodedAttributes(this.extension?.any ?: listOf())

fun AbstractDescriptor.codedStringAttributes(): List<CodedStringAttributeType> =
    findCodedAttributes(this.extension?.any ?: listOf())?.value?.codedStringAttribute ?: listOf()

fun AbstractDescriptor.codedDecimalAttributes(): List<CodedDecimalAttributeType> =
    findCodedAttributes(this.extension?.any ?: listOf())?.value?.codedDecimalAttribute ?: listOf()

fun AbstractDescriptor.codedIntegerAttributes(): List<CodedIntegerAttributeType> =
    findCodedAttributes(this.extension?.any ?: listOf())?.value?.codedIntegerAttribute ?: listOf()

private fun addCodedDecimalAttribute(
    extensionPoint: ExtensionType,
    codedDecimalAttribute: CodedDecimalAttributeType,
    mustUnderstand: Boolean
) {
    getOrCreateCodedAttributes(extensionPoint, mustUnderstand).apply {
        value.codedDecimalAttribute.add(codedDecimalAttribute)
    }
}

private fun addCodedIntegerAttribute(
    extensionPoint: ExtensionType,
    codedIntegerAttribute: CodedIntegerAttributeType,
    mustUnderstand: Boolean
) {
    getOrCreateCodedAttributes(extensionPoint, mustUnderstand).apply {
        value.codedIntegerAttribute.add(codedIntegerAttribute)
    }
}

private fun addCodedStringAttribute(
    extensionPoint: ExtensionType,
    codedStringAttribute: CodedStringAttributeType,
    mustUnderstand: Boolean
) {
    getOrCreateCodedAttributes(extensionPoint, mustUnderstand).apply {
        value.codedStringAttribute.add(codedStringAttribute)
    }
}

private fun getOrCreateCodedAttributes(
    extensionPoint: ExtensionType,
    mustUnderstand: Boolean
): JAXBElement<CodedAttributesType> {
    return findCodedAttributes(extensionPoint.any).let { attrs ->
        (attrs ?: ObjectFactory().createCodedAttributes(ObjectFactory().createCodedAttributesType()).also {
            extensionPoint.any.add(it)
        }).also {
            // the value of the coded attributes mustUnderstand flag is true if any of the attributes
            // is mustUnderstand = true
            it.value.isMustUnderstand = it.value.isMustUnderstand || mustUnderstand
        }
    }
}

private fun findCodedAttributes(any: List<Any>): JAXBElement<CodedAttributesType>? {
    return any.filterIsInstance<JAXBElement<CodedAttributesType>>().firstOrNull()
}