plugins {
    java
    application
    idea
    kotlin("jvm") version "1.9.22"
    id("maven-publish")
}

val thisVersion: String by project
val javaSource: String by project
val jdkVersion: String by project
val sdpiExtensionsModelVersion: String by project
val sdcRiVersion: String by project
val coroutinesVersion: String by project
val junitVersion: String by project
val sdcKtVersion: String by project
val protoSdcKtVersion: String by project
val daggerVersion: String by project
val ktorVersion: String by project

group = "org.somda.sdpi"

val isSnapshot: Boolean = project.hasProperty("snapshot")
val buildId: String? = System.getenv("CI_PIPELINE_IID")

group = "org.somda.protosdc.sdc"
version = when (isSnapshot) {
    true -> "${thisVersion}-SNAPSHOT" + buildId?.let { "+${buildId}" }
    false -> thisVersion
}

repositories {
    mavenCentral()
    // SDPi extensions model
    maven("https://gitlab.com/api/v4/projects/43332997/packages/maven")

    // SDC-kt
    maven("https://gitlab.com/api/v4/projects/48465392/packages/maven")

    // protoSDC-kt
    maven("https://gitlab.com/api/v4/projects/25130996/packages/maven")

    // protoSDC model and converter
    maven("https://gitlab.com/api/v4/projects/19759398/packages/maven")
    maven("https://gitlab.com/api/v4/projects/21967967/packages/maven")

    // SDCri snapshots
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
        mavenContent {
            snapshotsOnly()
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    // "Provides transitive vulnerable dependency maven:commons-collections:commons-collections:3.2.2"
    // that's ok, the model is not prone to the affected security risk as it does not use methods from
    // commons-collections directly
    implementation("org.somda.sdpi:extensions-model:$sdpiExtensionsModelVersion")
    implementation("org.somda.sdc:dpws:$sdcRiVersion")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")


    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")

    testImplementation("org.somda.protosdc.sdc:sdc-kt:$sdcKtVersion")
    testImplementation("org.somda.protosdc:biceps:$protoSdcKtVersion")
    testImplementation("org.somda.protosdc:crypto:$protoSdcKtVersion")
    testImplementation("org.somda.protosdc:common:$protoSdcKtVersion")
    testImplementation("org.somda.protosdc:sco:$protoSdcKtVersion")
    testImplementation("org.somda.protosdc:udp:$protoSdcKtVersion")

    testImplementation("com.google.dagger:dagger:$daggerVersion")
    testImplementation("io.ktor:ktor-server-core:$ktorVersion")
}

tasks.javadoc {
    if (JavaVersion.current().isJava9Compatible) {
        (options as StandardJavadocDocletOptions).addBooleanOption("html5", true)
    }
}

tasks.test {
    useJUnitPlatform()
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(javaSource)
    }
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(jdkVersion.toInt())
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}